#include<stdio.h>
#include<math.h>
int input()
{
	int a;
	scanf("%d",&a);
	return a;
}
int root1(int a, int b, int c)
{
	int d = (b^2) - (4*a*c);
	int r1 = (-b+(d^1/2))/(2*a);
	return r1;
}
int root2(int a, int b, int c)
{
	int d = (b^2)-(4*a*c);
	int r2 = (-b -(d^1/2))/(2*a);
	return r2;
}
void output(int r1, int r2)
{
	printf("Root 1 is : %d \n",r1);
	printf("Root 2 is : %d \n",r2);
}
void main()
{
	int a,b,c,r1,r2;
	printf("Enter the coefficients: \n");
	a = input();
	b = input();
	c = input();
	int d = (b^2) - (4*a*c);
	if (d>=0)
	{
		r1 = root1(a,b,c);
		r2 = root2(a,b,c);
		output(r1,r2);
	}
	else
	printf("Roots are imaginary");
}